# RLS

Lien vers la fiche questions/remarques : https://www.overleaf.com/9485851774mzbcbgdnfvjs

Lien vers le rapport de projet : https://www.overleaf.com/9413477858zqfmtrynjyrv

Lien vers la note d'étape : https://www.overleaf.com/9341229514xqmzfsgbzfpp

Groupe : 28

Nicolas  Andreani
Thomas Campolunghi
Robin Navarro

Sujet : Quelle méthode pour mesurer les effets de la réduction de loyer de solidarité (RLS) sur l'accès des ménages les plus modestes au logement ?