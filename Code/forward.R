install.packages('ISLR')
install.packages('leaps')
library(ISLR)
library(leaps)
library(MASS)


D_filtrer = ddeur_rls_2018[,c(2,3,6,7,9,11,18:19,24,28:29,35,47,49, 56, 57)]

mod0=lm(attribution_sous_1an ~ 1,data=D_filtrer)
modselect_f=stepAIC(mod0,attribution_sous_1an ~ 
    categorieSNE+ type_contrat_travail
    +type_lgt_recherche+nationalite+
    +ZoneABC_logement_recherch�+ Nature_demande+RLS+prioritaire+tranche_age,
    data= D_filter,trace=TRUE,direction=c("forward"))
summary(modselect_f)

modlin2=lm(attribution_sous_1an ~., D_filtrer) 
modselect_b=stepAIC(modlin2,~.,trace=TRUE, direction=c("backward"))
summary(modselect_b) 


#Remplacer les valeurs manquantes dans : montant_aide_lgt et loyer_max_supportable


m2018 <- glm(formula = attribution_sous_1an ~ ZoneABC_logement_recherch� + type_contrat_travail + 
               tranche_age + type_lgt_recherche + prioritaire + Nature_demande + 
               RLS + categorieSNE + taille_menage + nationalite, data = D_filtrer)
summary(m2018)

#retravailler les variables du/des mod�les pour diminuer le nombre de modalit�s car trop nombreuses et facilement regroupable.